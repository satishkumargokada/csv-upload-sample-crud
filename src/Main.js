
import React, { useState } from 'react';
import CSVReader from "react-csv-reader";
import "./styles.css";
import StoreTable from './StoreTable'


const Main = () => {
    const [tableData, setTableData] = useState([])
    const handleForce = (data, fileInfo) => {
        console.log(data, "data")
        setTableData(data)
    }

    const papaparseOptions = {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
    };
    return <div><CSVReader
        cssClass="react-csv-input"
        label="Select Store CSV File"
        onFileLoaded={handleForce}
        accept=".csv, text/csv"
        parserOptions={papaparseOptions}
    />
        {tableData.length > 0 ? <StoreTable tableData={tableData} /> : null}</div>

}
export default Main;

