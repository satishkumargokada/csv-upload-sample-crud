import { Table, Button, RangeSlider, Modal, ButtonToolbar } from 'rsuite';
import React, { useEffect, useState } from "react";


const StoreTable = ({ tableData }) => {
    const [tableHeaders, setTableHeaders] = useState([]);
    const [defaltRange, setDefaultRange] = useState(null);
    const [data, setData] = useState(tableData);
    const [delId, setDelId] = useState(null);
    const [originalData, setOriginalData] = useState(tableData);
    const [open, setOpen] = useState(false);
    const handleOpen = (id) => {
        setOpen(true);
        setDelId(id)
    };
    const handleYesClose = () => {
        const nextData = Object.assign([], data);
        let res = nextData.filter(item => item.id !== delId)
        setData(res);
        setOpen(false);
        setDelId(null)
    }
    const handleNoClose = () => {
        setOpen(false);
        setDelId(null)
    }
    useEffect(() => {
        setTableHeaders(Object.keys(data[0]));
        let arr = [Math.min(...data.map(item => item.price)), Math.max(...data.map(item => item.price))];
        setDefaultRange(arr)
    }, [data])

    const EditCell = ({ rowData, dataKey, onChange, ...props }) => {
        const editing = rowData.status === 'EDIT';
        return (
            <Table.Cell {...props} className={editing ? 'table-content-editing' : ''}>
                {editing ? (
                    <input
                        className="rs-input"
                        defaultValue={rowData[dataKey]}
                        onChange={event => {
                            onChange && onChange(rowData.id, dataKey, event.target.value);
                        }}
                    />
                ) : (
                    <span className="table-content-edit-span">{rowData[dataKey]}</span>
                )}
            </Table.Cell>
        );
    };

    const ActionCell = ({ rowData, dataKey, onClick, ...props }) => {
        return (
            <Table.Cell {...props} width={200} style={{ padding: '6px' }}>
                <Button
                    appearance="link"
                    onClick={() => {
                        onClick && onClick(rowData.id);
                    }}
                >
                    {rowData.status === 'EDIT' ? 'Save' : 'Edit'}
                </Button>
                <Button
                    appearance="link"
                    onClick={() => {
                        handleOpen(rowData.id)
                    }}
                >
                    Delete
                </Button>
            </Table.Cell>
        );
    };




    const handleChange = (id, key, value) => {
        const nextData = Object.assign([], data);
        nextData.find(item => item.id === id)[key] = value;
        setData(nextData);
    };
    const handleEditState = id => {
        const nextData = Object.assign([], data);
        const activeItem = nextData.find(item => item.id === id);
        activeItem.status = activeItem.status ? null : 'EDIT';
        setData(nextData);
    };





    return (
        <div>
            <Modal backdrop="static" role="alertdialog" open={open} onClose={handleNoClose} size="xs">
                <Modal.Body>
                    Are you sure you want to delete ?
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleYesClose} appearance="primary">
                        Yes
                    </Button>
                    <Button onClick={handleNoClose} appearance="subtle">
                        No
                    </Button>
                </Modal.Footer>
            </Modal>

            {defaltRange ? <RangeSlider defaultValue={defaltRange} onChange={val => {
                console.log(val)

                let res = originalData.filter(item => {
                    return parseFloat(item.price) > val[0] && parseFloat(item.price) < val[1]
                });
                if (res.length > 0) {
                    setData(res)
                }
            }} /> : null}
            <br />
            <Table
                height={400}
                data={data ? data : []}
                onRowClick={data => {
                    console.log(data);

                }}

            >

                {
                    tableHeaders.map(item => item != 'status' ? <Table.Column width={item == 'product_name' ? 150 : 100} align="center" key={item} fixed>
                        <Table.HeaderCell>{item.toUpperCase().replace('_', ' ')}</Table.HeaderCell>
                        <EditCell dataKey={item} onChange={handleChange} />
                    </Table.Column> : null)
                }

                <Table.Column width={120} fixed="right">
                    <Table.HeaderCell>Action</Table.HeaderCell>
                    <ActionCell dataKey="id" onClick={handleEditState} />

                </Table.Column>
            </Table></div>
    );
};
export default StoreTable;